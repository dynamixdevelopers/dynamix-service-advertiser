### Welcome
The `org.ambientdynamix.contextplugins.dnssd` plug-in when installed runs as a background service on the Ambient Dynamix Framework. It advertises a DNS Service of the type `_dynamix._tcp.local.` which any client on the local network can listen to. 

By default, the service is advertised for 60 seconds whenever

* the device connects to a new WiFi network
* `start` method is called on the `JmDNSRuntime` class. 

This duration can be changed by modifying the `advertisementDuration` variable in the `JmDNSRuntime` class.

### Dependencies
Starting API 16, Android provides the `NSDManager` class to advertise and search for services. However, to be able to support pre android 16 devices, this plug-in uses the [JmDNS](http://mvnrepository.com/artifact/com.github.rickyclarkson/jmdns/3.4.2-r353-1) library (The one on the JmDNS soureforge page is an older version).

### Context Support 
* `org.ambientdynamix.contextplugins.dnssd.advertise` Request the framework to advertise immediately and whenever the device connects to a new WiFi network. 

### Add context support for the plug-in.
* `contextHandler.addContextSupport("org.ambientdynamix.contextplugins.dnssd", "org.ambientdynamix.contextplugins.dnssd.advertise", callback, listener);`



