/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.nsd;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

public class JmDNSRuntime extends ContextPluginRuntime {

    Timer teardownTimer;
    long advertisementDuration = 60000;

    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    private BroadcastReceiver wifiStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "WiFi State Change");
            final String action = intent.getAction();
            if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo netInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (netInfo.isConnected() && isConnectedToWifi()) {
                    Log.d(TAG, "Connected to a WiFi Network");
                    advertiseDynamixService();
                } else {
                    Log.d(TAG, "Disconnected from a Network");
                    teardownJmDns();
                }
            }
        }
    };
    private String instanceId;

    private String type = "_dynamix._tcp.local.";
    private JmDNS jmdns = null;
    private ServiceInfo serviceInfo;
    android.net.wifi.WifiManager.MulticastLock lock;

    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        this.setPowerScheme(powerScheme);
        this.context = this.getSecuredContext();
        fetchInstanceId();
    }

    @Override
    public void start() {
        Log.d(TAG, "Started! JmDNSRuntime");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        advertiseDynamixService();
        context.registerReceiver(wifiStateReceiver, intentFilter);
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {
        Log.d(TAG, "Received new context request of type : " + contextType);
        if (contextType.equalsIgnoreCase(JmDNSInfo.CONTEXT_TYPE)) {
            advertiseDynamixService();
            sendContextRequestSuccess(requestId);
        } else {
            sendContextRequestError(requestId, "Context type not supported", ErrorCodes.NOT_SUPPORTED);
            Log.d(TAG, "Context type : " + contextType + " not supported");
        }
    }

    private void fetchInstanceId() {
        Bundle getInstanceId = new Bundle();
        getInstanceId.putString("OPERATION", "INSTANCE_ID");
        Result systemLevelOperationResult = dynamixSystemLevelOperation(getInstanceId);
        instanceId = systemLevelOperationResult.getMessage();
        Log.d(TAG, "Instance Id : " + instanceId);
    }

    private Boolean isConnectedToWifi() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    private void advertiseDynamixService() {
        Log.d(TAG, "Starting to advertise android dynamix service");
        new Thread(new Runnable() {

            @Override
            public void run() {
                WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                lock = wifi.createMulticastLock(getClass().getSimpleName());
                lock.setReferenceCounted(false);
                try {
                    InetAddress addr = getLocalIpAddress();
                    String hostname = addr.getHostName();
                    lock.acquire();
                    Log.d(TAG, "Ad" +
                            "dr : " + addr);
                    Log.d(TAG, "Hostname : " + hostname);
                    jmdns = JmDNS.create(addr, hostname);
                    serviceInfo = ServiceInfo.create(type,
                            "DynamixInstance", 7433,
                            "Service Advertisement for Ambient Dynamix");

                    HashMap<String, String> info = new HashMap<String, String>();
                    info.put("instanceId", instanceId);
                    serviceInfo.setText(info);
                    jmdns.registerService(serviceInfo);
                    Log.d(TAG, "Service Type : " + serviceInfo.getType());
                    Log.d(TAG, "Service Registration thread complete");

                    teardownTimer = new Timer();
                    teardownTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            teardownJmDns();
                        }
                    }, advertisementDuration);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }).start();
    }

    private void teardownJmDns() {
        if (teardownTimer != null) {
            teardownTimer.cancel();
            teardownTimer = null;
        }
        Log.d(TAG, "Tearing Down Service Advertisement");
        if (jmdns != null) {
            jmdns.unregisterAllServices();
            try {
                jmdns.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            jmdns = null;
        }
        //Release the lock
        if (lock != null)
            lock.release();
    }

    public InetAddress getLocalIpAddress() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();
        InetAddress address = null;
        try {
            address = InetAddress.getByName(String.format(Locale.ENGLISH,
                    "%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                    (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff)));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return address;
    }

    @Override
    public void stop() {
        try {
            context.unregisterReceiver(wifiStateReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Stopped!");
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        this.stop();
        context = null;
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
        sendContextRequestError(requestId, "Configured context request not supported", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {

    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {

    }


    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        return true;
    }

}